
// Maynards Site func :::::::::::::::::::::::::::::

var App = function(){

	$(document).foundation();

	// BIND FASTCLICK TO THE DELAYS ==============
	window.addEventListener('load', function () {
		FastClick.attach(document.body);
	}, false);

	console.log("site init")

}


// ===========================================
// Scale the FONT according to window size // 

$( document ).ready( function() {
        var $body = $('body'); //Cache this for performance
        
       	 var setBodyScale = function() {
          var scaleFactor = 0.05,
            scaleSource = $body.width(),
            maxScale = 600,
            minScale = 30;

         var windowW = $( window ).width();
         console.log(windowW)

          var fontSize = scaleSource * scaleFactor; //Multiply the width of the body by the scaling factor:

          if(windowW > 1000){
          	  if (fontSize > maxScale) fontSize = maxScale;
	          if (fontSize < minScale) fontSize = minScale; //Enforce the minimum and maximums

	          $('body').css('font-size', fontSize + '%');
          }
          else{
          	 $('body').css('font-size', 45 + '%');
          }
          
        }
      
          $(window).resize(function(){
          setBodyScale();
        });
        
        //Fire it when the page first loads:
        setBodyScale();
});



// Kick things off
var app = new App();